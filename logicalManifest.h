#ifndef LOGICAL_MANIFEST_H
#define LOGICAL_MANIFEST_H

#include <string>

class LogicalManifest
{
	private :

	std::string PU; // Textual purpose declaration
	unsigned int NbMapper; // Nombre de mapper
	unsigned int NbReducer; // Nombre de reducer
	std::string MapperRequest; // requete sql du mapper
	std::string MapperCode; // code du mapper
	std::string ReducerCode; // code du reducer
	std::string RootCode; // code de la racine
	unsigned int NbParticipant; // Nombre de participant
	unsigned int NbIteration; // Nombre d'itération pour le k-means

	public :

	// Constructeur
	LogicalManifest();
	LogicalManifest(std::string filename);

	// Getters
	unsigned int getNbMapper();
	unsigned int getNbReducer();
	std::string getMapperRequest();
	std::string getMapperCode();
	std::string getReducerCode();
	std::string getRootCode();
	unsigned int getNbIteration();

	// affichage des attributs de la classe
	void affiche_attributs();


};

#endif