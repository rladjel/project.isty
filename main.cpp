#include "logicalManifest.h"
#include "tools.h"
#include "function_set.h"
#include "mapper.h"
#include "reducer.h"
#include "root.h"

#include <iostream>

using namespace std;

int main(int argc, char * argv[])
{
	if(argc!=2)
	{
		cout << "Format attendu : ./interpreteur <fichier.lmf>" << endl;
		exit(EXIT_FAILURE);
	}

	// On vérifie que le fichier .lmf existe
	if(!exists_file(argv[1]))
	{
		cout << "Fichier non reconnu" << endl;
		exit(EXIT_FAILURE);
	}

	// On supprime l'ancien logfile si il existe
	if(exists_file("logfile.log") && remove("logfile.log")!=0)
	{
		cout << "Le fichier logfile.log n'a pas pu etre supprime" << endl;
		exit(EXIT_FAILURE);	
	}

	// Construction du manifeste
	// -------------------------------------------
	cout << "Lecture du Manifeste... " << endl;
	
	LogicalManifest LM(argv[1]);
	LM.affiche_attributs();
	// -------------------------------------------
	
    // Affectation des Mapper et Reducer
    // -------------------------------------------
	cout << "Affectation des rôles... " << endl;
	vector<vector<unsigned int>> matrice_roles = affecte_roles(LM.getNbMapper(), LM.getNbReducer());
	
	log_matrice_affectation(matrice_roles);
	// -------------------------------------------

	// Creation des Mappers
    // -------------------------------------------
	vector<Mapper *> v_mapper;
	v_mapper.resize(matrice_roles[0].size());

	for(unsigned int i = 0; i < v_mapper.size() ; i++)
	{
		v_mapper[i] = new Mapper(matrice_roles[0][i], LM);
	}
    // -------------------------------------------
	
    // Creation des Reducers
    // -------------------------------------------
	vector<Reducer * > v_reducer;
	v_reducer.resize(matrice_roles[1].size());
	for(unsigned int i = 0; i < v_reducer.size() ; i++)
		v_reducer[i] = new Reducer(matrice_roles[1][i], LM);
    // -------------------------------------------
    
	// Creation du Root
    // -------------------------------------------
	Root * racine = new Root(100, LM); // Ici on a choisit que le root porterait l'ID 100, a modifier au besoin
	// -------------------------------------------
	
	// On boucle selon le nombre d'iterations prevues dans le manifeste
	for(unsigned int i=0 ; i<LM.getNbIteration() ; i++)
	{
		cout << "Iteration " << i+1 << " :" <<endl;

		// Etape Map
		// -------------------------------------------
		cout << "Execution des mappers... " << endl;

		write_logs("Etape Map :\n");

		vector<key_value> mapper_results;
		for(unsigned int i = 0 ; i < v_mapper.size() ; i++)
			mapper_results.push_back(v_mapper[i]->map());
		// -------------------------------------------

		// Etape Shuffle
		// -------------------------------------------
		cout << "Shuffling... " << endl;

		vector<vector<key_value_set>> shuffle_results = shuffle_etape1(mapper_results, LM.getNbReducer());
		// -------------------------------------------

		// Etape Reduce
		// -------------------------------------------
		cout << "Execution des reducers... " << endl;

		write_logs("\nEtape Reduce :\n");
		
		vector<vector<key_value>> reducer_results;
		for(unsigned int i = 0 ; i < v_reducer.size() ; i++)
			reducer_results.push_back(v_reducer[i]->reduce(shuffle_results[i]));
		
		//log_reducer_results(reducer_results);
		// -------------------------------------------

		// Etape Root
		// -------------------------------------------
		cout << "Execution du root... " << endl;

		if(racine->root(reducer_results)==1)
			break;
		// -------------------------------------------

	}
	
	// Destruction des mappers reducers et root
	// -------------------------------------------
	for(unsigned int i = 0; i < v_mapper.size() ; i++)
	{
		delete v_mapper[i];
	}

	for(unsigned int i = 0; i < v_reducer.size() ; i++)
	{
		delete v_reducer[i];
	}

	delete racine;
	// -------------------------------------------

	return 0;
}