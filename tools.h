#ifndef TOOLS_H
#define TOOLS_H

#include <vector>
#include <string>

struct Point{
    float x;
    float y;
};

// Structure Key / Value 
struct key_value
{
   char * key;
   char * value;
};

// Structure Key / Ensemble de Value : (k, {v1,v2,v3})
struct key_value_set
{
   char * key;
   std::vector<char *> value_set;
};

// Fonction indicatrice de l'existance d'un fichier
bool exists_file(const std::string& name);

// Parseur de string avec un charactère en délimiteur
std::vector<std::string> split(const std::string& s, char delimiter);

// Parseur de string avec un string en délimiteur
std::vector<std::string> split(std::string stringToBeSplitted, std::string delimeter);

void write_logs(std::string logs);

char * encode_point(Point * P);
Point decode_point(char * bytes);

char * encode_int(int nb);
int decode_int(char * bytes);

std::string decode_string(char *bytes);

float dist(Point P1, Point P2);
Point barycentre(std::vector<Point> list_point);

std::string to_string_point(Point P);

std::string float2string(float f);
bool compare_point(Point p1, Point p2);

#endif