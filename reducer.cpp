#include "reducer.h"
#include "function_set.h"

#include <iostream>

using namespace std;

// Affichage des résulats du reducer
void log_reducer_etape1(key_value_set kvs, key_value kv)
{
	string logs = "";
	
	logs += "Key : " + decode_string(kvs.key) + "\n";

	logs += "Value Set : ";
	for(unsigned int i = 0 ; i < kvs.value_set.size() ; i++)
		logs += decode_string(kvs.value_set[i]) + " ";

	logs += "\nValue : " + decode_string(kv.value) + "\n";

	write_logs(logs);
}

// Affichage des résulats du reducer
void log_reducer_kmean(key_value_set kvs, key_value kv)
{
	string logs = "";
	
	logs += "Key : " + to_string_point(decode_point(kvs.key)) + "\n";

	logs += "Value Set : ";
	for(unsigned int i = 0 ; i < kvs.value_set.size() ; i++)
		logs += to_string_point(decode_point(kvs.value_set[i])) + " ";

	logs += "\nValue : " + to_string_point(decode_point(kv.value)) + "\n";

	write_logs(logs);
}

vector<key_value> reduce_etape1(vector<key_value_set> shuffling_results)
{
	vector<key_value> result;

	int somme;
	key_value kv;

	// On parcourt l'ensemble des clés que ce reducer doit traiter
	for(unsigned int i = 0 ; i < shuffling_results.size() ; i++)
	{
		somme = 0;

		// Pour chaque clé on somme toutes les values associées
		for(unsigned int j = 0 ; j < shuffling_results[i].value_set.size() ; j++)
			somme += decode_int(shuffling_results[i].value_set[j]);

		kv.key = shuffling_results[i].key;
		kv.value = encode_int(somme);

		result.push_back(kv);

		log_reducer_etape1(shuffling_results[i], kv);
	}

	return result;
}

vector<key_value> reduce_kmean(vector<key_value_set> shuffling_results)
{
	vector<key_value> reducer_results;
	vector<Point> list_point;
	key_value kv;

	for(unsigned int i=0 ; i<shuffling_results.size() ; i++)
	{
		kv.key = shuffling_results[i].key;

		for(unsigned int j=0 ; j<shuffling_results[i].value_set.size() ; j++)
			list_point.push_back(decode_point(shuffling_results[i].value_set[j]));

		Point bary = barycentre(list_point);
		kv.value = encode_point(&bary);

		reducer_results.push_back(kv);

		log_reducer_kmean(shuffling_results[i], kv);

		list_point.clear();
	}

	return reducer_results;	
}

Reducer::Reducer(unsigned int ID, LogicalManifest LM) : Node(ID, LM)
{
	string ReducerCode = LM.getReducerCode();
	char buffer[1024];


	//On affecte la bonne fonction au pointeur en regardant le contenu du manifest
	if (!ReducerCode.compare("reduce_etape1")) 
        reduce = reduce_etape1;
    else if (!ReducerCode.compare("reduce_kmean")) 
        reduce = reduce_kmean;
    else
    {
    	cout << "ReducerCode non reconnu" << endl;
    	exit(EXIT_FAILURE);
    }

    // Ici on initialise l'échange avec le seveur
    // ---------------------------------------------
    send(socket_fd, "2", strlen("2"), 0); // Le "2" indique au serveur qu'il s'agit d'un reducer

    bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }

    bzero(buffer, sizeof(buffer));

    sprintf(buffer, "%d", ID);
    send(socket_fd, buffer, strlen(buffer), 0);

    bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }
    // ---------------------------------------------
}