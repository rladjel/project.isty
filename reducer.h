#ifndef REDUCER_H
#define REDUCER_H

#include "node.h"
#include "tools.h"

// Pointeur de fonction reduce
typedef std::vector<key_value> (*treduce)(std::vector<key_value_set>);

// Fonctions d'affichage des resultats dans le log
void log_reducer_etape1(key_value_set kvs, key_value kv);
void log_reducer_kmean(key_value_set kvs, key_value kv);

std::vector<key_value> reduce_etape1(std::vector<key_value_set> shuffling_results);
std::vector<key_value> reduce_kmean(std::vector<key_value_set> shuffling_results);

class Reducer : public Node 
{
	public :

	treduce reduce;

	Reducer(unsigned int ID, LogicalManifest LM);
};

#endif