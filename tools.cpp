#include "tools.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

bool exists_file (const string& name) 
{
  	ifstream f(name.c_str());

  	if(f.good())
  	{
  		f.close();
  		return true;
  	}
    return false;
}


vector<string> split(const string& s, char delimiter)
{
   vector<string> tokens;
   string token;
   istringstream tokenStream(s);
   while (getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

vector<string> split(string stringToBeSplitted, string delimeter)
{
     vector<string> splittedString;
     unsigned int startIndex = 0;
     unsigned int  endIndex = 0;
     while( (endIndex = stringToBeSplitted.find(delimeter, startIndex)) < stringToBeSplitted.size() )
    {
       string val = stringToBeSplitted.substr(startIndex, endIndex - startIndex);
       splittedString.push_back(val);
       startIndex = endIndex + delimeter.size();
     }
     if(startIndex < stringToBeSplitted.size())
     {
       string val = stringToBeSplitted.substr(startIndex);
       splittedString.push_back(val);
     }
     return splittedString;
}

void write_logs(string logs)
{
  ofstream logfile("logfile.log", ios::out | ios::app);
  if(!logfile)
  {
    cout << "Impossible d'ouvrir le fichier de log" << endl;
    exit(EXIT_FAILURE);
  }

  logfile << logs;

  logfile.close();
}

char * encode_point(Point * P)
{
	char * tmp = reinterpret_cast<char*>(P);
	char * res = (char *)calloc(sizeof(tmp),sizeof(char));

	for(unsigned int i=0 ; i<sizeof(tmp) ; i++)
		res[i] = tmp[i];

    return res;
}

Point decode_point(char * bytes)
{
    Point P;

    char * x_bytes = (char *)malloc(4*sizeof(char));
    char * y_bytes = (char *)malloc(4*sizeof(char));
    
    for(int i = 0 ; i < 4 ; i++)
    { 
        x_bytes[i] = bytes[i];
        y_bytes[i] = bytes[i+4];
    }

    float * x = reinterpret_cast<float*>(x_bytes);
    float * y = reinterpret_cast<float*>(y_bytes);

    P.x = *x;
    P.y = *y;

    return P;
}

char* encode_int(int nb) 
{
  char * s = (char *)calloc(11,sizeof(char));
  sprintf(s,"%d", nb);
  return s;
}

int decode_int(char * bytes)
{
  return atoi(bytes);
}

string decode_string(char *bytes)
{
  string s(bytes);
  return s;
}

float dist(Point P1, Point P2)
{
  return sqrt(((P1.x-P2.x)*(P1.x-P2.x)) + ((P1.y-P2.y)*(P1.y-P2.y)));
}

Point barycentre(vector<Point> list_point)
{
  Point P;

  float somme_x = 0;
  float somme_y = 0;

  for(unsigned int i=0; i<list_point.size();i++)
  {
    somme_x += list_point[i].x;
    somme_y += list_point[i].y;
  }

  P.x = somme_x / list_point.size();
  P.y = somme_y / list_point.size();

  return P;
}

string to_string_point(Point P)
{
  return "(" + to_string(P.x) + " ; " + to_string(P.y) + ")";
}

string float2string(float f)
{
  ostringstream os;
  os << f;
  return os.str();
}

bool compare_point(Point p1, Point p2)
{
	int x1,x2,y1,y2;

	x1 = (int)p1.x*1000000;
	y1 = (int)p1.y*1000000;
	x2 = (int)p2.x*1000000;
	y2 = (int)p2.y*1000000;

	if(x1==x2 && y1==y2)
		return true;
	
	return false;
}