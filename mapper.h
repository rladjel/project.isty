#ifndef MAPPER_H
#define MAPPER_H

#include "node.h"
#include "tools.h"


void log_mapper_etape1(key_value kv, int id);
void log_mapper_kmean(key_value kv, int id);


class Mapper : public Node 
{

	private:

	key_value map_etape1();
	key_value map_kmean();
	void emit(key_value kv);

	public :

	key_value map();

	Mapper(unsigned int ID, LogicalManifest LM);
};

#endif