#include "mapper.h"
#include "function_set.h"

#include <iostream>

using namespace std;

// Fonctions d'affichage des resultats des mappers
// ---------------------------------------
void log_mapper_etape1(key_value kv, int id)
{
	string logs = "";

	logs += "Mapper " + to_string(id) + " : ";
	logs += "Key : " + decode_string(kv.key);
	logs += " ; Value : " + decode_string(kv.value) + "\n";

	write_logs(logs);
}

void log_mapper_kmean(key_value kv, int id)
{
	string logs = "";

	logs += "Mapper " + to_string(id) + " : ";
	logs += "Key : " + to_string_point(decode_point(kv.key));
	logs += " ; Value : " + to_string_point(decode_point(kv.value)) + "\n";

	write_logs(logs);
}
// ---------------------------------------

key_value Mapper::map_etape1()
{
	key_value kv;

	string query = LM.getMapperRequest();

	// on ajoute le code spécifique au participant
	query.append("\nWHERE id = " + to_string(ID) + ";");

	vector<char *> res = exec_query(query);

	kv.key = res[0];
	kv.value = res[1];

	log_mapper_etape1(kv, ID);

	return kv;
}

key_value Mapper::map_kmean()
{
	key_value kv;

	string query = LM.getMapperRequest();

	// on ajoute le code spécifique au participant
	query.append("\nWHERE id = " + to_string(ID) + ";");

	vector<Point> B = recupere_barycentres(3);
	vector<char *> res = exec_query(query);

	Point P;
	P.x = atof(res[1]);
	P.y = atof(res[2]);

	float d_min = 10000;
	unsigned int indice = 0;
	float d = 0;

	for(unsigned int i = 0; i<B.size(); i++)
	{
		d=dist(B[i],P);

		if(d<d_min)
		{
			d_min = d;
			indice = i;
		}
	}

	kv.key = encode_point(&B[indice]);
	kv.value = encode_point(&P);

	log_mapper_kmean(kv, ID);

	return kv;
}

void Mapper::emit(key_value kv)
{
	unsigned int id_reducer;

	char buffer[1024];

	// Envoi de l'ID du reducer à qui envoyer les données
	// --------------------------------------------------
	id_reducer = hashage(kv.key, LM.getNbReducer());

	sprintf(buffer, "%d", id_reducer);

	send(socket_fd, buffer, strlen(buffer), 0);
	// --------------------------------------------------

	bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }

    // Envoi de la clé 
    send(socket_fd, kv.key, sizeof(kv.key), 0);

    bzero(buffer, sizeof(buffer));

    // Accusé de reception
    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }

    // Envoi de la valeur
    send(socket_fd, kv.value, sizeof(kv.value), 0);

    bzero(buffer, sizeof(buffer));

    // Accusé de reception
    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }

}

key_value Mapper::map()
{
	string MapperCode = LM.getMapperCode();

	key_value kv;

	// On lance la fonction map indiqué dans le manifestze
	if (!MapperCode.compare("map_etape1")) 
        kv = map_etape1();
    else if (!MapperCode.compare("map_kmean")) 
        kv = map_kmean();
    else
    {
    	cout << "MapperCode non reconnu" << endl;
    	exit(EXIT_FAILURE);
    }


    emit(kv); // emission au serveur

    return kv;
}

Mapper::Mapper(unsigned int ID, LogicalManifest LM) : Node(ID, LM)
{
	char buffer[1024];

    send(socket_fd, "1", strlen("1"), 0); // le "1" indique au serveur qu'il s'agit d'un mapper

    bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }

    bzero(buffer, sizeof(buffer));

    // Envoi de l'identifiant du mapper 
    sprintf(buffer, "%d", ID);
    send(socket_fd, buffer, strlen(buffer), 0);

    bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }
}