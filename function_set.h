#ifndef FUNCTION_SET_H
#define FUNCTION_SET_H

#include "tools.h"
#include <fstream>
#include <vector>
#include <string>

// Fonction d'affectation "basique" des mappers et des reducers
std::vector<std::vector<unsigned int>> affecte_roles(unsigned int NbMapper, unsigned int NbReducer);

// Logs des affectations
void log_matrice_affectation(std::vector<std::vector<unsigned int>> matrice_roles);

// Execution d'une requête sur la BD
std::vector<char *> exec_query(std::string query);
std::vector<Point> recupere_barycentres(unsigned int NbBarycentre);
void update_barycentre(Point bary, Point p);

// Suffle à l'étape 1
std::vector<std::vector<key_value_set>> shuffle_etape1(std::vector<key_value> mapper_results, unsigned int NbReducer);

// Fonction de hashage "basique" (modulo)
unsigned int hashage(char * key, unsigned int NbReducer);


#endif