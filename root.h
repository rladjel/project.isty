#ifndef ROOT_H
#define ROOT_H

#include "node.h"
#include "tools.h"

typedef int (*troot)(std::vector<std::vector<key_value>>);

int root_etape1(std::vector<std::vector<key_value>> reducer_results);
int root_groupby(std::vector<std::vector<key_value>> reducer_results);
int root_kmean(std::vector<std::vector<key_value>> reducer_results);

class Root : public Node 
{
	public :

	troot root;

	Root(unsigned int ID, LogicalManifest LM);	
};

#endif