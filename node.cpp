#include "node.h"

Node::Node(unsigned int ID, LogicalManifest LM)
{
	this->ID = ID;
	this->LM = LM;

	/* Initialise IPv4 server address with server host. */
    memset(&server_address, '\0', sizeof server_address);
    
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(PORT);
    server_address.sin_addr.s_addr = inet_addr("127.0.0.1");

    /* Create TCP socket. */
    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    /* Connect to socket with server address. */
    if (connect(socket_fd, (struct sockaddr *)&server_address, sizeof server_address) == -1) {
		perror("connect");
        exit(1);
	}
}

Node::~Node()
{
    send(socket_fd, "exit", strlen("exit"), 0);
	close(socket_fd);
}