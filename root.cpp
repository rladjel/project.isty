#include "root.h"
#include "function_set.h"
#include <iostream>

using namespace std;

int root_etape1(vector<vector<key_value>> reducer_results)
{
	int somme = 0;

	// Pour chaque reducer
	for(unsigned int i = 0 ; i < reducer_results.size() ; i++)
	{
		// Pour chaque Key traiter par le reducer i
		for(unsigned int j = 0 ; j < reducer_results[i].size() ; j++)
		{
			// on somme les values
			somme += decode_int(reducer_results[i][j].value);
		}
	}

	cout << "Resultat à transmettre au Querier : " << somme << endl;

	return 0;
}
int root_groupby(vector<vector<key_value>> reducer_results)
{
	cout << "Resultat à transmettre au Querier : " << endl;

	// Pour chaque reducer
	for(unsigned int i = 0 ; i < reducer_results.size() ; i++)
	{
		// Pour chaque Key traiter par le reducer i
		for(unsigned int j = 0 ; j < reducer_results[i].size() ; j++)
		{
			cout << "Key : " << decode_int(reducer_results[i][j].key) << " Value : " << decode_int(reducer_results[i][j].value) << endl;
		}
	}

	return 0;
}
int root_kmean(std::vector<std::vector<key_value>> reducer_results)
{
	int cpt=0;
	int nb_conv=0;

	// Pour chaque reducer
	for(unsigned int i = 0 ; i < reducer_results.size() ; i++)
	{
		// Pour chaque Key traiter par le reducer i
		for(unsigned int j = 0 ; j < reducer_results[i].size() ; j++)
		{
			/*size_t size = sizeof(reducer_results[i][j].key);
			if(!memcmp(reducer_results[i][j].key,reducer_results[i][j].value,size))
				nb_conv++;*/

			if(compare_point(decode_point(reducer_results[i][j].key),decode_point(reducer_results[i][j].value)))
				nb_conv++;
			else
				update_barycentre(decode_point(reducer_results[i][j].key),decode_point(reducer_results[i][j].value));
			cpt++;
		}
	}

	if(cpt==nb_conv)
	{
		cout << "convergence" << endl;
		return 1; // On retourne 1 pour indiquer au main qu'il faut arreter les iterations
	}

	return 0;
}

Root::Root(unsigned int ID, LogicalManifest LM) : Node(ID, LM)
{
	string RootCode = LM.getRootCode();
	char buffer[1024];

	// On affecte la bonne fonction au pointeur
	if (!RootCode.compare("root_etape1")) 
        root = root_etape1;
    else if (!RootCode.compare("root_groupby")) 
    	root = root_groupby;
    else if (!RootCode.compare("root_kmean")) 
    	root = root_kmean;
    else
    {
    	cout << "RootCode non reconnu" << endl;
    	exit(EXIT_FAILURE);
    }

    // Initialisation de la communication avec le serveur
    // --------------------------------------------------
    send(socket_fd, "3", strlen("3"), 0); // le "3" indique au serveur que ce noeud est le root

    bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }

    bzero(buffer, sizeof(buffer));

    sprintf(buffer, "%d", ID);
    send(socket_fd, buffer, strlen(buffer), 0);

    bzero(buffer, sizeof(buffer));

    if(recv(socket_fd, buffer, 1024, 0) < 0){
        printf("[-]Error in receiving data.\n");
    }
    else{
        printf("Server : %s\n", buffer);
    }
    // --------------------------------------------------
}