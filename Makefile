CXX=g++
CPPFLAGS = -L/usr/lib/mysql
LDLIBS = -lmysqlclient
CXXFLAGS = -std=c++11 -Wall
EXEC = interpreteur server

all : $(EXEC)

interpreteur : tools.o logicalManifest.o function_set.o node.o mapper.o reducer.o root.o main.o
	$(CXX) $(CXXFLAGS) -o $@ $^ $(CPPFLAGS) $(LDLIBS)

server : server.c
	gcc -Wall -pthread -lnsl $< -o $@

main.o : tools.h logicalManifest.h function_set.h mapper.h reducer.h root.h

%.o : %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $< 

clean :
	rm -rf *.o

mrproper :
	rm -rf $(EXEC)
	rm -rf logfile.log