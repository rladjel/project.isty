# Projet ISTY - Calcul sur une architecture PDMS

 - Encadré par :  Riad Ladjel, Nicolas Anciaux, Guillaume Scerri, Philippe Pucheral
 - Réalisé par :  Vicnesh Selva, Ludovic Javet

### Installation des librairies

Pour connecter le code C++ au serveur MySQL : 
```
$ sudo apt-get install libmysqlclient-dev
```

Voir (https://openclassrooms.com/fr/courses/1101991-utiliser-lapi-mysql-dans-vos-programmes?fbclid=IwAR0mKFTTQsG7b9EXSiga23MMyOtmec1eF3VNTEjj1S_nm2DK4nSuXUESO9o)

### Création de la table participant dans MySQL

- Créer la base de données "projet_pdms"
- Exécuter le code sql contenu dans "projet_pdms.sql"

### Compilation

Pour compiler le projet :
```
$ make
```
### Exécution

Il faut d'abord lancer le server pour que les mappers reducers et root puissent s'y connecter :
```
$ ./server &
```

Pour exécuter le projet en utilisant le manifeste de l'étape 1 : 
```
$ ./interpreteur etape1.lmf
```