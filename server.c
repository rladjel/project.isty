#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define BACKLOG 10
#define PORT 4444

pthread_mutex_t mutex;

typedef struct pthread_arg_t {
    int new_socket_fd;
    struct sockaddr_in client_address;
} pthread_arg_t;

/* Thread routine to serve connection to client. */
void *pthread_routine(void *arg);

int main(int argc, char *argv[]) {
    int socket_fd, new_socket_fd;
    struct sockaddr_in address;
    pthread_attr_t pthread_attr;
    pthread_arg_t *pthread_arg;
    pthread_t pthread;
    socklen_t client_address_len;

    /* Initialise IPv4 address. */
    memset(&address, 0, sizeof address);
    address.sin_family = AF_INET;
    address.sin_port = htons(PORT);
    address.sin_addr.s_addr = INADDR_ANY;

    /* Create TCP socket. */
    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    /* Bind address to socket. */
    if (bind(socket_fd, (struct sockaddr *)&address, sizeof address) == -1) {
        perror("bind");
        exit(1);
    }

    /* Listen on socket. */
    if (listen(socket_fd, BACKLOG) == -1) {
        perror("listen");
        exit(1);
    }

    /* Initialise pthread attribute to create detached threads. */
    if (pthread_attr_init(&pthread_attr) != 0) {
        perror("pthread_attr_init");
        exit(1);
    }
    if (pthread_attr_setdetachstate(&pthread_attr, PTHREAD_CREATE_DETACHED) != 0) {
        perror("pthread_attr_setdetachstate");
        exit(1);
    }

    while (1) {
        /* Create pthread argument for each connection to client. */
        pthread_arg = (pthread_arg_t *)malloc(sizeof *pthread_arg);
        if (!pthread_arg) {
            perror("malloc");
            continue;
        }

        /* Accept connection to client. */
        client_address_len = sizeof pthread_arg->client_address;
        new_socket_fd = accept(socket_fd, (struct sockaddr *)&pthread_arg->client_address, &client_address_len);
        if (new_socket_fd == -1) {
            perror("accept");
            free(pthread_arg);
            continue;
        }

        /* Initialise pthread argument. */
        pthread_arg->new_socket_fd = new_socket_fd;

        /* Create thread to serve connection to client. */
        if (pthread_create(&pthread, &pthread_attr, pthread_routine, (void *)pthread_arg) != 0) {
            perror("pthread_create");
            free(pthread_arg);
            continue;
        }
    }
    close(socket_fd);

    return 0;
}

void *pthread_routine(void *arg) {

    char buffer[1024];

    int type=0;
    int ID=0;
    
    pthread_arg_t *pthread_arg = (pthread_arg_t *)arg;
    int new_socket_fd = pthread_arg->new_socket_fd;
    free(arg);

    while(1){

        bzero(buffer, sizeof(buffer));

        recv(new_socket_fd, buffer, 1024, 0);

        if(strcmp(buffer, "exit") == 0)
            break;
        
        if(type==0)
        {
            // On récupère le type du client (1, 2 ou 3, mapper, reducer ou root)
            int tmp = atoi(buffer);
            type = tmp;
            send(new_socket_fd, "ack", strlen("ack"), 0);
        }
        else if(ID==0)
        {  
            // On recupère l'ID du noeud
            int tmp = atoi(buffer);
            ID = tmp;
            send(new_socket_fd, "ack", strlen("ack"), 0);
        }
        else
        {
            // A poursuivre, ici je me contente d'afficher le contenu de ce que m'envoie les nodes
            switch(type)
            {
                case 1:
                    printf("Mapper (%d) : %s\n", ID, buffer);
                    send(new_socket_fd, "ack", strlen("ack"), 0);
                    break;
                case 2:
                    printf("Reducer (%d) : %s\n", ID, buffer);
                    send(new_socket_fd, "ack", strlen("ack"), 0);
                    break;
                case 3:
                    printf("Root (%d) : %s\n", ID, buffer);
                    send(new_socket_fd, "ack", strlen("ack"), 0);
                    break;
                default:
                    perror("type non reconnu");
                    exit(1);
            }
        }
    }
    close(new_socket_fd);
    return NULL;
}
