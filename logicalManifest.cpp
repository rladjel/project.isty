#include "logicalManifest.h"
#include "tools.h"

#include <iostream>
#include <fstream>
#include <streambuf>
#include <vector>

using namespace std;

// Manifeste vide 
LogicalManifest::LogicalManifest()
{
	PU = "";
	NbMapper = 0;
	NbReducer = 0;
	MapperRequest = "";
	MapperCode = "";
	ReducerCode = "";
	RootCode = "";
	NbParticipant = 0;
	NbIteration = 0;
}

LogicalManifest::LogicalManifest(string filename)
{
	// Ouverture du fichier
	ifstream file(filename);

	// L'intégralité du fichier est transformé en string
	string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());

	// Fermeture du fichier
	file.close();

	// On parse le fichier avec "\n@\n" comme délimiteur
	vector<string> v_str= split(str,"\n@\n");

	// On vérfie la conformité du fichier
	if(v_str.size()!=9)
	{
		cout << "Erreur : Le fichier \""+filename+"\" ne respecte pas le bon format" << endl;
		exit(EXIT_FAILURE);
	}

	// On affecte le contenu du fichier aux différents attributs
	PU = v_str[0];
	NbMapper = stoul(v_str[1]);
	NbReducer = stoul(v_str[2]);
	MapperRequest = v_str[3];
	MapperCode = v_str[4];
	ReducerCode = v_str[5];
	RootCode = v_str[6];
	NbParticipant = stoul(v_str[7]);
	NbIteration = stoul(v_str[8]);
}

unsigned int LogicalManifest::getNbMapper() { return NbMapper; }
unsigned int LogicalManifest::getNbReducer() { return NbReducer; }
string LogicalManifest::getMapperRequest() { return MapperRequest; }
string LogicalManifest::getMapperCode() { return MapperCode; }
string LogicalManifest::getReducerCode() { return ReducerCode; }
string LogicalManifest::getRootCode() { return RootCode; }
unsigned int LogicalManifest::getNbIteration() { return NbIteration; }

void LogicalManifest::affiche_attributs()
{
	cout << endl << "************* LogicalManifest *************" << endl;

	cout << "PU : " << endl;;
	cout << PU << endl;

	cout << "NbMapper : " << endl;;
	cout << NbMapper << endl;

	cout << "NbReducer : " << endl;;
	cout << NbReducer << endl;

	cout << "MapperRequest : " << endl;;
	cout << MapperRequest << endl;

	cout << "MapperCode : " << endl;;
	cout << MapperCode << endl;

	cout << "ReducerCode : " << endl;;
	cout << ReducerCode << endl;

	cout << "RootCode : " << endl;;
	cout << RootCode << endl;

	cout << "NbParticipant : " << endl;;
	cout << NbParticipant << endl;

	cout << "NbIteration : " <<endl;
	cout << NbIteration << endl;

	cout << "*******************************************" << endl << endl;
}