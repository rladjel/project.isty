#ifndef NODE_H
#define NODE_H

#include "logicalManifest.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 4444 

class Node
{
	protected :

	unsigned int ID;
	LogicalManifest LM;

	int socket_fd;
    struct sockaddr_in server_address;

	public :

	Node(unsigned int ID, LogicalManifest LM);
	~Node();

};

#endif