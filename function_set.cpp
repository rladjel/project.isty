#include "function_set.h"

#include <iostream>
#include <algorithm>
#include <cstring>

// Librairies connection MySQL
#include <arpa/inet.h>
#include <mysql/mysql.h>

using namespace std;

// Fonction permettant de donner un identifiant aux mappers reducers (par rapport à la table participant)
vector<vector<unsigned int>> affecte_roles(unsigned int NbMapper, unsigned int NbReducer)
{
	vector<vector<unsigned int>> matrice_roles;
	vector<unsigned int> mappers;
	vector<unsigned int> reducers;

	int ID = 1;

	// Affectations des reducers
	for(unsigned int i = 0 ; i < NbReducer ; i++)
	{
		reducers.push_back(ID);
		ID++;
	}

	// et des mappers
	for(unsigned int i = 0 ; i < NbMapper ; i++)
	{
		mappers.push_back(ID);
		ID++;
	}
	
	matrice_roles.push_back(mappers);
	matrice_roles.push_back(reducers);

	return matrice_roles;
}

// Affichage dans le fichier de log
void log_matrice_affectation(vector<vector<unsigned int>> matrice_roles)
{
	unsigned int i;

	string logs = "";

	logs += "Matrice d'affectation : \n";

	logs += "Mappers :\n";
	logs += "[";

	for(i = 0 ; i < (matrice_roles[0].size() - 1); i++)
		logs += to_string(matrice_roles[0][i]) + ", ";

	if(matrice_roles[0].size()>0)
		logs += to_string(matrice_roles[0][i]) ;
	
	logs += "]\n";

	logs += "Reducers :\n";
	logs += "[";

	for(i = 0 ; i < (matrice_roles[1].size() - 1); i++)
		logs += to_string(matrice_roles[1][i]) + ", ";

	if(matrice_roles[1].size()>0)
		logs += to_string(matrice_roles[1][i]) ;

	logs += "]\n\n";

	write_logs(logs);
}

// exectution d'une requete dans la BD
vector<char *> exec_query(string query)
{
	vector<char *> res;

	unsigned int num_champs = 0;

	MYSQL mysql;
	mysql_init(&mysql);

	mysql_options(&mysql, MYSQL_READ_DEFAULT_GROUP, "option");

	// Connection à la BD
	if(mysql_real_connect(&mysql, "localhost","root","","projet_pdms",3306,"/opt/lampp/var/mysql/mysql.sock",0) == NULL)
	{
		cout <<"Echec connexion :" << endl;
		cout << mysql_error(&mysql) << endl << endl << endl;
		exit(EXIT_FAILURE);
	}
	
	 //Requête qui sélectionne tout dans ma table scores
    mysql_query(&mysql, query.c_str());

    //Déclaration des objets
    MYSQL_RES *result = NULL;
    MYSQL_ROW row;

    //On met le jeu de résultat dans le pointeur result
    result = mysql_use_result(&mysql);

    num_champs = mysql_num_fields(result);

    if(num_champs <= 1)
    {
    	cout << "Erreur : le nombre de champs renvoyés par la requete est inférieur ou égal à 1" << endl;
    	exit(EXIT_FAILURE);
    }

    row = mysql_fetch_row(result);

    // On récupère le résultat dans notre structure Key/Value

    for(unsigned int i=0 ; i<num_champs ; i++)
    {
    	size_t size = sizeof(row[i]);
    	char * tmp = (char *)calloc(size,sizeof(char));
    	strncpy(tmp,row[i],size);
    	res.push_back(tmp);
    }

    //Libération du jeu de résultat
    mysql_free_result(result);

    //Fermeture de MySQL
    mysql_close(&mysql);

    return res;
}

vector<Point> recupere_barycentres(unsigned int NbBarycentre)
{
	vector<Point> res;
	Point P;
	unsigned int num_champs = 0;

	MYSQL mysql;
	mysql_init(&mysql);

	mysql_options(&mysql, MYSQL_READ_DEFAULT_GROUP, "option");

	if(mysql_real_connect(&mysql, "localhost","root","","projet_pdms",3306,"/opt/lampp/var/mysql/mysql.sock",0) == NULL)
	{
		cout <<"Echec connexion :" << endl;
		cout << mysql_error(&mysql) << endl << endl << endl;
		exit(EXIT_FAILURE);
	}
	
	string query = "SELECT x,y FROM barycentre WHERE id<=" + to_string(NbBarycentre) + ";";
    mysql_query(&mysql, query.c_str());

    //Déclaration des objets
    MYSQL_RES *result = NULL;
    MYSQL_ROW row;

    //On met le jeu de résultat dans le pointeur result
    result = mysql_use_result(&mysql);

    num_champs = mysql_num_fields(result);

    if(num_champs != 2)
    {
    	cout << "Erreur : le nombre de champs renvoyés par la requete est different de 2" << endl;
    	exit(EXIT_FAILURE);
    }

    while ((row = mysql_fetch_row(result)))
    {
    	P.x = (float)atoi(row[0])/1000000; // ajustement des données contenues dans la table SQL
    	P.y = (float)atoi(row[1])/1000000;
    	res.push_back(P);
    }

    if(res.size() != NbBarycentre)
    {
    	cout << "Erreur : le nombre de barycentre renvoyés par la requete est different de " << NbBarycentre << endl;
    	exit(EXIT_FAILURE);
    }

    //Libération du jeu de résultat
    mysql_free_result(result);

    //Fermeture de MySQL
    mysql_close(&mysql);

    return res;
}

void update_barycentre(Point bary, Point p)
{
	MYSQL mysql;
	mysql_init(&mysql);

	mysql_options(&mysql, MYSQL_READ_DEFAULT_GROUP, "option");

	if(mysql_real_connect(&mysql, "localhost","root","","projet_pdms",3306,"/opt/lampp/var/mysql/mysql.sock",0) == NULL)
	{
		cout <<"Echec connexion :" << endl;
		cout << mysql_error(&mysql) << endl << endl << endl;
		exit(EXIT_FAILURE);
	}
	
	string query = "UPDATE barycentre SET x = " + to_string((int)(p.x*1000000)) + ",y = " + to_string((int)(p.y*1000000));
	query += " WHERE x = " + to_string((int)(bary.x*1000000)) + " AND y = " + to_string((int)(bary.y*1000000)) + ";";

	cout << query << endl;

    int success = mysql_query(&mysql, query.c_str());

    //Fermeture de MySQL
    mysql_close(&mysql);

    if(success != 0)
    {
    	cout << "Erreur : update des barycentres a echoue" << endl;
    	exit(EXIT_FAILURE);
    }
}


vector<vector<key_value_set>> shuffle_etape1(vector<key_value> mapper_results, unsigned int NbReducer)
{
	unsigned int i,j;

	vector<vector<key_value_set>> shuffle_results;

	key_value kv;
	unsigned int current_reducer;

	shuffle_results.resize(NbReducer);
	// On parcourt l'ensemble des résultats du mapping
	for(i = 0 ; i < mapper_results.size() ; i++)
	{
		kv = mapper_results[i]; // On  mémorise la clé valeur courant
		size_t size_key = sizeof(kv.key);
		current_reducer = hashage(kv.key, NbReducer); // On hash la clé pour connaitre le reducer à qui envoyer les données

		// Vérifions si le reducer en question ne possède pas déjà cette clé
		// si NbReducer grand : modifier la structure de shuffle_results pour éviter de faire ça (Voir Hashmap)
		for(j = 0 ; j < shuffle_results[current_reducer].size() ; j++)
			if (!memcmp(shuffle_results[current_reducer][j].key,kv.key,size_key))
				break;

		// si le reducer ne possède aucun key_value_set ou ne trouve pas la clé alors il faut construire un key_value_set et lui donner
		if (shuffle_results[current_reducer].empty() || j >= shuffle_results[current_reducer].size())
		{
			key_value_set kvs;
			kvs.key = kv.key;
			kvs.value_set.push_back(kv.value);
			shuffle_results[current_reducer].push_back(kvs);
		}
		else // sinon le reducer possède la clé donc il faut ajouter la value à son value_set
		{
			shuffle_results[current_reducer][j].value_set.push_back(kv.value);
		}
	}

	return shuffle_results;
}

unsigned int hashage(char * key, unsigned int NbReducer)
{
    unsigned int somme = 0;

    for(unsigned int i = 0 ; i<sizeof(key) ; i++)
        somme += key[i];
   	// Un simple modulo pour l'instant
    return somme%NbReducer;
}
